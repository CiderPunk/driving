import { PlayerCar } from "./ents/playercar";
import { Road } from "./road/road";
import { Light, DirectionalLight, HemisphericLight } from "@babylonjs/core/Lights";
import { Scene } from "@babylonjs/core/scene"
import { Engine } from "@babylonjs/core/Engines/engine";
import { FreeCamera, TargetCamera } from "@babylonjs/core/Cameras"
import { Vector3, Color3, Matrix } from "@babylonjs/core/Maths/math";
import { AssetsManager } from "@babylonjs/core/Misc"
import { AssetContainer } from "@babylonjs/core/assetContainer";
import { CubeTexture } from "@babylonjs/core/Materials/Textures/cubeTexture";
import { Map1 } from "./data/map1";
import { IRoadDataCollection } from "./interface";
import * as planck from "planck-js"
import { Vec2 } from "planck-js";
import { DebugRender } from "./ui/debugrender";
import { Command, ControlMan } from "./controlman";
import * as Constants from "./constants";
import * as MathHelper from "./utils/mathhelper";
import { SignBase } from "./ents/signbase";


export const PauseToggle =  new Command("PAUSE_TOGGLE","PAUSE",27)
export const DebugToggle =  new Command("DEBUG_TOGGLE","DEBUG",113)

export class Game{

  readonly canvas: HTMLCanvasElement;
  readonly engine: Engine;
  private scene:Scene
  private camera:TargetCamera
  private light:Light
  private reqAnimFrame:number
  private lastFrame:number = 0

  private speed:number = 300
  private road:Road

  private position:number = 0

  protected readonly assMan:AssetsManager
    //asset container
  protected readonly container:AssetContainer
  hdrTexture: CubeTexture;
  protected mapData:IRoadDataCollection
  protected readonly world:planck.World;


  private debugRender:DebugRender
  player: PlayerCar;
  protected physicsTime:number


  
  protected doFrame():void{
    //get time and last time..
    const  t:number = performance.now() / 1000
    const dT = t- this.lastFrame
    this.lastFrame = t
    
    if (t - this.physicsTime > 0.2){ //if physics falls too far behind renderin' skip some steps
      this.physicsTime = t
    }

    this.position = this.player.getPosition()
    this.speed = this.player.getSpeed()
    //queue up next frame
    this.reqAnimFrame = window.requestAnimationFrame(()=>{this.doFrame()})
    //this.position+=dT * this.player.speed// this.speed
    //this.camera.position.set(0,10,-15+this.camPos)
    this.road.update(this.position)

    this.updateLights(this.position)
    this.player.preDraw(dT)


    this.camera.position.x = MathHelper.lerp(this.camera.position.x, -this.player.getXPosition(), 0.1)

    //draw the frame
    this.engine.beginFrame()
    this.scene.render()
    this.engine.endFrame()

    this.debugRender.draw(this.world)

    while (this.physicsTime < t){
      this.physicsTime += Constants.PhysicsTickTime 
      this.player.prePhysics(this.mapData.horizontal)
      this.world.step(Constants.PhysicsTickTime)
      this.player.postPhysics()
    }

  }

  private hdrRotation:number = 0 
  private lightStart:Vector3 = new Vector3(0.6,1,0.2)

  updateLights(position:number) {
    //move the light
    const light = this.light as DirectionalLight
    const turn = this.mapData.horizontal.turnAtAccurate(position) * 0.000025 * this.speed
    this.hdrRotation += turn
    const rotationMatrix = Matrix.RotationY(this.hdrRotation)
    light.direction = Vector3.TransformCoordinates(this.lightStart, rotationMatrix)
    this.hdrTexture.setReflectionTextureMatrix( rotationMatrix)     
  }


  public constructor(canvasId:string, debugCanvasId:string = "debug"){

    ControlMan.getInstance().registerCommands([PauseToggle, DebugToggle])
    this.canvas = document.getElementById(canvasId) as HTMLCanvasElement
    this.engine = new Engine(this.canvas) 

    this.scene= new Scene(this.engine)
    const assMan = new AssetsManager(this.scene)
    this.container = new AssetContainer(this.scene)
    
    this.world = new planck.World()

    // The canvas/window resize event handler.
    window.addEventListener("resize", () => {
      this.engine.resize();
    })

    this.loadAssets(assMan)
    
    assMan.load()
    this.world = new planck.World({ gravity:new planck.Vec2(0, 0), allowSleep:false, warmStarting:false  })


    assMan.onFinish = ()=>{ 
      this.container.moveAllFromScene()
      this.initPhysics()
      this.initScene()

      this.reqAnimFrame = window.requestAnimationFrame(()=>{this.doFrame()})

      this.mapData = new Map1(this.scene)
      //road controller
      this.road = new Road(this.scene, "Road1", this.mapData)
    
      this.initEnts()
      this.physicsTime = 0
    }

    this.debugRender = new DebugRender(debugCanvasId)
    DebugToggle.Subscribe((active)=>{  if (active){ this.debugRender.toggleVis() }})
  }


  initEnts() {
    //player?
    this.player = new PlayerCar(this.scene, this.world)
  }

  private initScene(){

    this.hdrTexture = CubeTexture.CreateFromPrefilteredData("assets/envSpecularHDR.dds", this.scene)
    this.hdrTexture.gammaSpace = false
    this.scene.environmentTexture = this.hdrTexture

    // Create a FreeCamera, and set its position to (x:0, y:5, z:-10).
    this.camera =  new TargetCamera('camera1', new Vector3(0,12,-24), this.scene)
    this.camera.fov = 1
    // Target the camera to scene origin.
    this.camera.setTarget(new Vector3(0,0,40))
    // Attach the camera to the canvas.
    this.camera.attachControl(this.canvas, false)

    // Create a basic light, aiming 0,1,0 - meaning, to the sky.
    this.light = new HemisphericLight('light1', new Vector3(0.6,-1,0.2).normalize(), this.scene)

    //this.light = new DirectionalLight('light1', new Vector3(0.6,-1,0.2).normalize(), this.scene)
    this.light.falloffType = Light.FALLOFF_STANDARD
    this.light.intensity = 1


    //fog
    this.scene.fogMode = Scene.FOGMODE_EXP;
    this.scene.fogDensity = 0.005;
    this.scene.fogColor= new Color3(0.6, 0.6, 0.7)

 

  }

  private loadAssets(assMan:AssetsManager){
    PlayerCar.LoadAssets(assMan)
    SignBase.LoadAssets(assMan)



  }


  leftSide:planck.Body
  rightSide:planck.Body

  initPhysics():void {
    //create world model add cars and crap. 
    this.leftSide = this.world.createBody({type:'static', position:planck.Vec2(-26,0)})
    this.leftSide.createFixture({ shape: planck.Edge( Vec2(0,-400), Vec2(0,400))})
    this.rightSide = this.world.createBody({type:'static', position:planck.Vec2(26,0)})
    this.rightSide.createFixture({ shape: planck.Edge( Vec2(0,-400), Vec2(0,400))})
  }
  


}
