export function clamp(value:number, min:number, max:number):number{
  return Math.min(Math.max(value, min), max)
}

export function lerp(start:number, finish:number, mix:number):number{
  return start + ((finish - start) * clamp(mix,0,1))
}
