export  async function getJSON(path:string, callback:(data:any)=>void):Promise<void> {
  return callback(await fetch(path).then(r => r.json()));
}