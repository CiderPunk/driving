export interface INode<T>{
  value:T,
  next?:INode<T>
}


export class LinkedList<T>{

  protected head:INode<T> = null
  protected tail:INode<T> = null

  public constructor(items?:Array<T>){
    if (items){
      this.addArray(items)
    }
  }

  get isEmpty():boolean{
    return !this.head
  }

  public add(item:T):LinkedList<T>{
    const node = {value:item, next:null} as INode<T>
    if (this.isEmpty){
      this.head = node    
    }
    else{
      this.tail.next = node
    }
    this.tail = node
    return this
  }

  public addArray(items:Array<T>):LinkedList<T>{
    items.forEach((item)=>this.add(item))
    return this
  }


  public every(fn:(T)=>void){
    const itr = this.items()
    let item = itr.next()
    while(!item.done){
      fn(item.value)
      item = itr.next()
    }
  }


  //for infinite looings of nodes
  public *loopNodes(){
    let node  = this.head
    while(node){
      yield node
      node = node.next ?? this.head
    }
  }


  public *loopItemsFrom(startNode:INode<T>){
    let node =startNode
    while(node){
      yield node.value
      node = node.next ?? this.head
    }
  }

  //for infinite looings of items
  public *loopItems(){
    let node  = this.head
    while(node){
      yield node.value
      node = node.next ?? this.head
    }
  }


  public *items(){
    let node  = this.head
    while(node){
      yield node.value
      node = node.next
    }
  }

}