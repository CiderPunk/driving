import { Mesh } from "@babylonjs/core/Meshes/mesh";
import { Texture } from "@babylonjs/core/Materials/Textures/texture";

export interface IRoadDataCollection{
  horizontal:IRoadData
  vertical:IRoadData
  styles:Map<string, IRoadStyle> 
}

export interface IRoadStyle{
  init(turnMap:Texture, drawDist:number)
  update(offset:number)
  readonly roadMesh:Mesh
}



export interface IRoadData{
  turnAtAccurate(position:number):number
  turnAt(position:number):number
  getTurnMap(position:number, count:number, writeStep:number, writeOffset:number, data:Float32Array)
}