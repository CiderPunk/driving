import { IRoadDataCollection, IRoadStyle } from "../interface";
import { RoadData } from "../road/roaddata";
import { RoadStyle } from "../road/roadstyle";
import { Scene } from "@babylonjs/core/scene";

export class Map1 implements IRoadDataCollection{
  readonly horizontal = new RoadData([
    {length:100,turn:0, easing:100},  
    {length:300,turn:5, easing:300},  
    {length:50,turn:0, easing:50},
    {length:100,turn:-20, easing:100},
    {length:500,turn:0, easing:500},
    {length:100,turn:10, easing:100},

    {length:200,turn:5, easing:60},
    {length:200,turn:-5, easing:60},
    {length:200,turn:5, easing:60},
    {length:200,turn:-5, easing:60},
  ])
  readonly vertical = new RoadData([
    {length:100,turn:10, easing:60},  
    {length:300,turn:0, easing:30},  
    {length:50,turn:5, easing:30},
    {length:100,turn:0, easing:30}, 
    {length:500,turn:-2, easing:30},
    {length:100,turn:0, easing:30},
    {length:800,turn:0, easing:30},
  ])
  readonly styles:Map<string, IRoadStyle>

  constructor(scene:Scene){
    this.styles = new Map<string, IRoadStyle>()
    this.styles.set("default", new RoadStyle(scene, {}))
  }


}