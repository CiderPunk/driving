
export interface ITurnSegment{
  turn:number,
  length:number,
  easing:number,
}

export interface IStyleSegment{
  length:number,
  style:string
}


export interface IRoadStyleDef{ 
  name?:string,
  laneCount?:number
  laneWidth?:number
  rumbleWidth?:number
  lineWidth?:number
  segmentLength?:number
  rumbleColPri?:number[]
  rumbleColAlt?:number[]
  roadColPri?:number[]
  roadColAlt?:number[]
  grassColPri?:number[]
  grassColAlt?:number[]
  lineCol?:number[],
  segmentCount?:number
}





export interface ILevel{
  horizontal:Array<ITurnSegment>
  vertical:Array<ITurnSegment>
  styles:Array<IRoadStyleDef>
}



export class Defaults{

  public static readonly defaultStyle:IRoadStyleDef ={
    name:"default",
    laneCount:3,
    laneWidth:15,
    rumbleWidth:5,
    lineWidth:0.5,
    segmentCount:240,
    rumbleColPri:[1,0,0,1],
    rumbleColAlt:[1,1,1,1],
    roadColPri:[0.2,0.2,0.2,1],
    roadColAlt:[0.3,0.3,0.3,1],
    grassColPri:[0.2,0.8,0.2,1],
    grassColAlt:[0.3,1,0.3,1],
    lineCol:[1,1,1,1],
  }
}
