
export type CommandAction  = (active:boolean,name:string)=>void;


export class Command{
  public constructor(private name:string, private action:string, private defaultKeyCode:number){  }
  private actions = new Array<CommandAction>();
  private active:boolean = false
  //default key code of command
  get DefaultKeyCode():number{
    return this.defaultKeyCode
  }
  //unique name of command
  get Name():string{
    return this.name
  }
  //short non-unique name
  get Action():string{
    return this.action
  }
  //is active
  get IsActive():boolean{
    return this.active
  }

  public Subscribe(act:CommandAction){
    if (this.actions.indexOf(act) == -1){
      this.actions.push(act)
    }
  }

  public Unsubscribe(act:CommandAction){
    let index = this.actions.indexOf(act)
    if (index > -1){
      this.actions.splice(index, 1)
    }
  }

  public Start(){
    if (!this.active){
      this.actions.forEach((act)=>act(true, this.action))
      this.active = true
    }
  }

  public Stop(){
    this.active = false
    this.actions.forEach((act)=>act(false, this.action))
  }
}

/*
interface CommandMap {
  [name: string]: Command;
}
interface KeyMap {
  [keyCode: number]: Command;
}
*/

export class ControlMan{

  keyMap:Map<Number, Command> = new Map<Number,Command>()
  commandMap:Map<string,Command> = new Map<string,Command>()

  private constructor(){
    document.addEventListener("keydown", (e)=>{ this.keyDownHandler(e) }, false)
    document.addEventListener("keyup",  (e)=>{ this.keyUpHandler(e) }, false)
  }

  private static _instance:ControlMan

  static getInstance():ControlMan {
    return ControlMan._instance ?? (ControlMan._instance = new ControlMan())
  }

  
  protected keyDownHandler(e:KeyboardEvent):void{
    console.log("key down:" + e.keyCode)
    const command = this.keyMap.get(e.keyCode) as Command
    if (command){
      console.log(command.Name +" start")
      command.Start()
    }
  }

  protected keyUpHandler(e:KeyboardEvent){
    //console.log("key up:" + e.keyCode);
    const command = this.keyMap.get(e.keyCode) as Command
    if (command){
      console.log(command.Name +" stop")
      command.Stop()
    }
  }

  public bind(keyCode:number, commandName:string):void{
    let command = this.commandMap.get(commandName) as Command
    if (command){
      this.keyMap.set(keyCode, command)
    }
  }


  public registerCommands(commands: Command[]):void{
    for (let command of commands){
      this.registerCommand(command)
    }
  }

  public registerCommand(command: Command):void{
    if (!this.commandMap.has(command.Name)){
      this.commandMap.set(command.Name, command)
      //attempt to fecth binding from config..
      //TODO:cofig stuff
      //fail - use defauilt
      this.bind(command.DefaultKeyCode, command.Name)
    }
    else{
      throw new Error("Duplicate command registered: " + command.Name)
    }
  }
  public deRegisterCommand(command: Command){
    this.commandMap.delete(command.Name)
  }


}

