

export class DebugRender{

  private readonly canvas:HTMLCanvasElement
  private readonly ctx: CanvasRenderingContext2D

  private visible:boolean 

  public toggleVis():void{
    this.visible = !this.visible
    this.canvas.style.display = this.visible ? "block" : "none"
  }

  public constructor(canvasId:string){
    this.canvas = document.getElementById(canvasId) as HTMLCanvasElement
    this.ctx = this.canvas.getContext('2d')
  
    this.canvas.width = this.canvas.clientWidth
    this.canvas.height = this.canvas.clientHeight

    this.visible = true
    this.toggleVis()
    
    // The canvas/window resize event handler.
    window.addEventListener("resize", () => {
      this.canvas.width = this.canvas.clientWidth
      this.canvas.height = this.canvas.clientHeight
    })
  }


  public draw(world:planck.World): void {

    const ctx = this.ctx
    //clear canvas
    ctx.setTransform(1,0,0,1,0, 0);
    this.ctx.clearRect(0,0,this.canvas.width, this.ctx.canvas.height);
    ctx.setTransform(-4,0,0,-4,this.canvas.width / 2, this.canvas.height / 2);
    ctx.strokeStyle = "pink"
    ctx.lineWidth = 1



    for(var body = world.getBodyList(); body; body = body.getNext() ){
      if (body.isActive()){
        ctx.save()
        ctx.translate(body.getPosition().x, body.getPosition().y)
        ctx.rotate(body.getAngle())
        for(var fix = body.getFixtureList(); fix; fix = fix.getNext()){
          var shape = fix.getShape();
          switch(shape.m_type){
            case  "circle":
              var circle = shape as planck.CircleShape

              const center = circle.getCenter()
              //ctx.moveTo(center.x, center.y)
              ctx.beginPath()
              ctx.strokeStyle = "red"
              ctx.arc(center.x, center.y, circle.m_radius , 0,2*Math.PI)
              ctx.lineTo(center.x,center.y)
              ctx.stroke()
              break;

            case "polygon":
              var poly = shape as planck.PolygonShape
              var verts = poly.m_vertices
              var last = verts[verts.length - 1]
              ctx.beginPath()
              ctx.strokeStyle = "green"
              ctx.moveTo(last.x, last.y);
              for(var point of poly.m_vertices){
                ctx.lineTo(point.x,point.y)
              }
              ctx.stroke()
              break
            case "edge":
              var edge = shape as planck.EdgeShape
              ctx.beginPath()
              ctx.strokeStyle = "purple"
              ctx.moveTo( edge.m_vertex1.x, edge.m_vertex1.y)
              ctx.lineTo(edge.m_vertex2.x, edge.m_vertex2.y)
              ctx.stroke()
              break

            case "chain":
              var chain = shape as planck.ChainShape
              var verts = chain.m_vertices
              ctx.moveTo(verts[0].x, verts[0].y)
              ctx.beginPath()
              ctx.strokeStyle = "blue"      
              for(var point of verts)  {
                ctx.lineTo(point.x,point.y)
              }
              ctx.stroke()
              break
          }
        }
        ctx.restore()
      }
    }
  }



}