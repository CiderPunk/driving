import { IRoadData } from "../interface"
import * as MathHelper from "../utils/mathhelper"
import { ITurnSegment } from "../leveldata"

export class RoadData implements IRoadData{

  private readonly grainSize = 8

  readonly roadLength:number

  readonly turnMap:Array<number>

  constructor( segments:Array<ITurnSegment>){
    //get total length
    let length = 0
    segments.forEach(s=>{ length+= s.length })
    this.roadLength = length
    //get number of grains
    const count = Math.ceil(this.roadLength / this.grainSize)
    this.turnMap = new Array<number>(count)

    let lastTurn = segments[segments.length - 1].turn
    let currentSegment = 0
    let position = 0
    for (let i = 0; i < count; i++){
      //check if we've finished the current segment
      if (position > segments[currentSegment].length){
        //decrease the position
        position -= segments[currentSegment].length
        //store the last turn for easing
        lastTurn = segments[currentSegment].turn
        //get the next segment
        currentSegment++  
        //loop back if we go to far
        if (currentSegment > segments.length -1){
          currentSegment = 0
        }
      }
      //get easing ratio
      let t = position / segments[currentSegment].easing
      //lerp between previous turn and new based on the above squared
      this.turnMap[i] =  MathHelper.lerp(lastTurn, segments[currentSegment].turn, t * t)
      //move along to the next grain
      position += this.grainSize
    }
  }

  /**
   * returns turn of the specified grain index
   * @param index 
   */
  protected getGrainTurn(index:number):number{ 
    return this.turnMap[index % this.turnMap.length]
  }

  /**
   * gets turn lerp'd between grains at the specified poixel position
   * @param position 
   */
  public turnAtAccurate(position:number):number{
    const grain = Math.floor(position / this.grainSize)
    const last = this.getGrainTurn(grain)
    const current = this.getGrainTurn(grain+1)
    return last == current ? last : MathHelper.lerp(last,current, position % this.grainSize / this.grainSize)
  }

  /**
   * gets turn at nearest grain for the pixel position in the map
   * @param position 
   */
  public turnAt(position:number):number{
    const actualPos = position % this.roadLength
    return this.getGrainTurn[Math.round(actualPos / this.grainSize)]
  }



  /**
   * generates turn texture for shader use, the returned array are deviations from the center
   * @param position pixel position of view in the map
   * @param count number of pixels to draw ahead in the Z direction (away)
   * @param writeStep //write every 3rd value to target RGB or 4th to generate RGBA
   * @param writeOffset // offset to start writing at, 0 = R, 1 = G, 2 = B
   * @param data //float array to write to.
   */
  public getTurnMap(position:number, count:number, writeStep:number, writeOffset:number = 0, data:Float32Array){
    //const actualPos = position % this.roadLength
    let offset = position % this.grainSize
    let grainIndex  = Math.floor(position / this.grainSize)
    //get prev grain turn
    let last = this.getGrainTurn(grainIndex)
    //get current grain turn, inc grain index
    let current = this.getGrainTurn(++grainIndex)
    let x = 0.5 // start in the middle
    let dX = 0 //rate of change starts at zero
    for(let i = 0; i < count; i++){
      //if there's a variation between last and current...
      //lerp between them otherwise don't bother
      dX += (last == current ? last : MathHelper.lerp(last,current, offset / this.grainSize)) * 0.00005
      x += dX
      //store our offset
      data[writeOffset + (writeStep * i)] = x
      //check if we finished this grain, advance the index, get new last and current turn rates
      if (++offset > this.grainSize){
        offset -= this.grainSize
        last = current
        current = this.getGrainTurn(++grainIndex)
      }
    }



  }


}