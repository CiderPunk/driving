
import { Scene } from "@babylonjs/core/scene"
import { MeshBuilder, VertexData } from "@babylonjs/core/Meshes";
import { Mesh } from "@babylonjs/core/Meshes/mesh";
import { StandardMaterial } from "@babylonjs/core/Materials"
import { Texture, RawTexture } from "@babylonjs/core/Materials/Textures"
import { CustomMaterial } from "@babylonjs/materials"
import { Engine } from "@babylonjs/core/Engines/engine";
import { Vector3 } from "@babylonjs/core/Maths/math";

import { float } from "@babylonjs/core/types";

import "@babylonjs/core/Meshes/meshBuilder"
import { IRoadDataCollection, IRoadStyle } from "../interface";
import * as Constants from "../constants";


export  class Road{


  private turnMap:RawTexture
  readonly roadDrawCycle:number = 4 * Constants.SegmentLength
  readonly drawDist:number = 2048
  private ready:boolean

  private turnMapBuffer:Float32Array
  protected cyclePosition:float = 0
  activeRoadStyle:IRoadStyle



  public constructor(protected readonly scene:Scene, public readonly name:string, protected readonly mapData:IRoadDataCollection){
    this.turnMapBuffer = new Float32Array(3 * this.drawDist)

    for (let i =0; i < this.turnMapBuffer.length; i++){
      this.turnMapBuffer[i] = 0.5
    }

    this.turnMap = RawTexture.CreateRGBTexture(this.turnMapBuffer, this.drawDist, 1, this.scene, false, false, Texture.LINEAR_NEAREST, Engine.TEXTURETYPE_FLOAT )

    this.mapData.styles.forEach((rs:IRoadStyle)=>{ rs.init(this.turnMap, this.drawDist )})


    //test mesh to see turn texture
    const testmesh = MeshBuilder.CreateGround("test", {width:256, height:256, subdivisions:1, updatable:false}, this.scene)
    testmesh.position = new Vector3(0,-20,0)
    const testmat = new StandardMaterial("testmat", this.scene)
    testmat.emissiveTexture = this.turnMap
    testmat.disableLighting = true
    testmesh.material = testmat

    //this.roadMaterial.onBind=()=>{this.ready = true}
    this.ready = true
    this.activeRoadStyle = mapData.styles.get("default")
  }

  public update(position:number){

    if (this.ready){

      //update turn map buffer with currtent turns
      this.mapData.horizontal.getTurnMap(position, this.drawDist, 3,0, this.turnMapBuffer)
      this.mapData.vertical.getTurnMap(position, this.drawDist, 3,1, this.turnMapBuffer)

      //update the turnmap texture
      this.turnMap.update(this.turnMapBuffer)

      //update the draw cycle offset
      this.cyclePosition = position % this.roadDrawCycle
      this.activeRoadStyle.update( this.cyclePosition)

    }
  }




}