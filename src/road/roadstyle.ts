import { Scene } from "@babylonjs/core/scene";
import { Mesh, VertexData } from "@babylonjs/core/Meshes";
import { CustomMaterial } from "@babylonjs/materials";
import { Texture } from "@babylonjs/core/Materials";
import * as Constants from "../constants";
import { Defaults, IRoadStyleDef  } from "../leveldata";
import { IRoadStyle } from "../interface";

export  class RoadStyle implements IRoadStyle{

/*
  static readonly defaultOpts:IRoadOptions = {
    laneCount:3,
    laneWidth:15,
    rumbleWidth:5,
    lineWidth:0.5,
    segmentCount:240,
    rumbleColPri:[1,0,0,1],
    rumbleColAlt:[1,1,1,1],
    roadColPri:[0.2,0.2,0.2,1],
    roadColAlt:[0.3,0.3,0.3,1],
    grassColPri:[0.2,0.8,0.2,1],
    grassColAlt:[0.3,1,0.3,1],
    lineCol:[1,1,1,1],
  }
*/
  public readonly roadMesh: Mesh
  protected offset:number

  public constructor(protected readonly scene:Scene, protected opts:IRoadStyleDef){
    opts = {...Defaults.defaultStyle, ...opts}
    this.roadMesh = this.SegmentBuilder(scene, name, opts)
    this.roadMesh.position.set(0,0,-30)
  }

  public update(offset:number){
    this.offset = offset
  }

  public init(turnMap:Texture, drawDist:number){
    this.roadMesh.material = this.buildRoadShader(turnMap, drawDist)
  }

  
  private SegmentBuilder(scene:Scene, name:string, options:IRoadStyleDef):Mesh{
    
    const roadWidth = ((options.laneCount - 1) * options.lineWidth) + (options.laneCount * options.laneWidth)
    const totalWidth = (2 * options.rumbleWidth) + roadWidth

    const verts:number[] = []
    const cols:number[] = []
    const indices:number[] = []

    //start on the left
    let start = -totalWidth/2

    for (let i = 0; i < options.segmentCount; i++){
      //sides 
      this.addSquare(verts,cols,indices, start, i * Constants.SegmentLength, -4000, Constants.SegmentLength, 0,  (i % 2 ?options.grassColAlt : options.grassColPri))
      this.addSquare(verts,cols,indices, -start, i * Constants.SegmentLength, 4000, Constants.SegmentLength, 0,  (i % 2 ?options.grassColAlt : options.grassColPri))

      //left rumble
      this.addSquare(verts, cols, indices, start , i * Constants.SegmentLength, options.rumbleWidth, Constants.SegmentLength, 0, (i % 2 ?options.rumbleColAlt : options.rumbleColPri))
      this.addSquare(verts, cols, indices, -start , i * Constants.SegmentLength, -options.rumbleWidth, Constants.SegmentLength, 0, (i % 2 ?options.rumbleColPri : options.rumbleColAlt))
      //draw road without dividers
      this.addSquare(verts, cols, indices, start + options.rumbleWidth, i * Constants.SegmentLength, roadWidth, Constants.SegmentLength, 0, (i % 2  ?options.roadColAlt : options.roadColPri ))
    }
    start += options.rumbleWidth 

    //draw lane dividers
    for (let i = 1; i < options.laneCount; i++){
  
      for (let j = 0; j < options.segmentCount /4; j++){
        
        this.addSquare(verts, cols, indices, start + (i * ( options.laneWidth + options.lineWidth)) ,  (j * 4 * Constants.SegmentLength), options.lineWidth, Constants.SegmentLength, 0.02, options.lineCol)
        this.addSquare(verts, cols, indices, start + (i * ( options.laneWidth + options.lineWidth)) ,  (((j * 4) + 1) * Constants.SegmentLength), options.lineWidth, Constants.SegmentLength, 0.02, options.lineCol)
      }
    } 

    const segmentMesh = new Mesh(name, scene )
    const vertexData = new VertexData()
    const normals:number[] = []
    VertexData.ComputeNormals(verts, indices, normals);
    vertexData.positions = verts
    vertexData.indices = indices
    vertexData.colors = cols
    vertexData.normals = normals
    vertexData.applyToMesh(segmentMesh)
    
    return segmentMesh
  }
  
  private addSquare(verts:number[], vertCols:number[], indicies:number[], x:number,y:number, width:number, height:number, z:number, colour:number[]):void{
    const start = verts.length / 3
    verts.push(x,z,y,
      x+width,z,y,
      x+width,z,y+height,
      x,z,y+height)
    for (let i = 0; i < 4; i++){
      vertCols.push(...colour)
    }
    //indicies
    if (width * height > 0){
      indicies.push(start , start + 1, start +2, start, start +2, start+3)
    } 
    else{ //reverse direction maybe...
      indicies.push(start , start + 2, start +1, start, start +3, start+2)
    }
  }

  private buildRoadShader(turnMap:Texture, drawDist:number):CustomMaterial{
    const roadShader = new CustomMaterial("roadShader", this.scene)

    roadShader.AddUniform("uRoadOffset", "float",null)
    roadShader.AddUniform("uTurnMap", "sampler2D", null)
    roadShader.AddUniform("uDistToTexel", "float", null) 
    roadShader.AddUniform("uModelOffset", "float", null) 

    roadShader.Vertex_Before_PositionUpdated(`
      vec3 v = position;
      v.z = v.z - uRoadOffset;
      vec4 turndata = texture2D(uTurnMap, vec2(clamp((v.z + uModelOffset) * uDistToTexel,0.,1.) , 0.5));
      v.x += (turndata.x -0.5) * 10.;
      v.y += (turndata.y - 0.5) * 10.;    
      positionUpdated=v;
    `)

    roadShader.onBind =()=>{ 
      roadShader.getEffect().setTexture("uTurnMap", turnMap)
      roadShader.getEffect().setFloat("uDistToTexel", 1 / drawDist)
      roadShader.getEffect().setFloat("uModelOffset", -30)
    }  

    roadShader.onBindObservable.add(()=>{
      roadShader.getEffect().setFloat("uRoadOffset", this.offset)
    })
    return roadShader 
  }


}