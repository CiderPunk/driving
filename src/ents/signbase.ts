import { AssetsManager } from "@babylonjs/core/Misc"
import { AbstractMesh } from "@babylonjs/core/Meshes"

export class SignBase{

  protected static curveMesh:AbstractMesh
  protected static warnMesh:AbstractMesh
  protected static circleMesh:AbstractMesh



  static LoadAssets(assMan:AssetsManager){
    const task = assMan.addMeshTask("loadSigns","","assets/", "signs.gltf")
    task.onSuccess = (task)=>{ 
      SignBase.curveMesh = task.loadedMeshes.filter(v=>v.id == "curve")[0]
      SignBase.warnMesh = task.loadedMeshes.filter(v=>v.id == "warning")[0]
      SignBase.circleMesh = task.loadedMeshes.filter(v=>v.id == "circle")[0]


    }
  } 

}