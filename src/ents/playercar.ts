import { AssetsManager } from "@babylonjs/core/Misc"
import { AbstractMesh } from "@babylonjs/core/Meshes/abstractMesh";
import { Scene } from "@babylonjs/core/scene";
import { Vector3 } from "@babylonjs/core/Maths/math";
  
import "@babylonjs/core/Loading"
import "@babylonjs/loaders/glTF"
import { PBRMaterial } from "@babylonjs/core/Materials";
import { Command, ControlMan } from "../controlman";

import { Body, Vec2, Box } from "planck-js";
import { IRoadData } from "../interface";
import * as MathHelper from "../utils/mathhelper";


export const PlayerLeft =  new Command("P1_TURN_LEFT","LEFT",37)
export const PlayerRight = new Command("P1_TURN_RIGHT","RIGHT",39)
export const PlayerAccelerate = new Command("P1_ACCELERATE","ACC", 38) 
export const PlayerBrake = new Command("P1_BRAKE","BRAKE",40)


export class PlayerCar{

  protected position:number = 0
  protected turnDirection:number = 0
  protected turnRate:number = 0
  protected static carMesh:AbstractMesh
  protected body: Body
  protected acc: number = 0
  playermesh: AbstractMesh
  rearWheels: AbstractMesh[]
  frontWheels:AbstractMesh[]
  wheelPos: number = 0
  


  static readonly baseName = "lambomesh"
  static entCount:number = 0

  private readonly id:string
  constructor(scene:Scene, world:planck.World){
    this.id = PlayerCar.baseName + (++PlayerCar.entCount)
    
    ControlMan.getInstance().registerCommands([ PlayerLeft, PlayerRight, PlayerAccelerate, PlayerBrake])
    this.playermesh = PlayerCar.carMesh.clone( this.id, scene.rootNodes[4])
    this.frontWheels = this.playermesh.getChildMeshes(true, t=>t.id.startsWith(this.id + ".Wheel_F"))
    this.rearWheels = this.playermesh.getChildMeshes(true, t=>t.id.startsWith(this.id + ".Wheel_R"))




    //scene.addMesh(playermesh)
    this.playermesh.scaling = new Vector3(0.36,0.36,0.36)
    this.playermesh.position = new Vector3(0,0,0)
    this.body = world.createDynamicBody(new Vec2(0,0),0)
    this.body.createFixture({ shape: Box(2,4), density:0.2})


    PlayerLeft.Subscribe((active:boolean)=>{ 
      this.turnDirection+= (active? -1: 1)
    })
    PlayerRight.Subscribe((active:boolean)=>{
      this.turnDirection+= (active? 1: -1)  
    })
    PlayerAccelerate.Subscribe((active:boolean)=>{
      this.acc += active ? 1 : -1
    })

    PlayerBrake.Subscribe((active:boolean)=>{
      this.acc += active ? -2 : 2
    })
  }


  public getPosition():number{
    return this.position
  }
  public getXPosition():number{
    return this.body.getPosition().x
  }


  public getSpeed():number{
    return this.body.getLinearVelocity().y
  }

  public preDraw(dT:number){
    const pos = this.body.getPosition()
    this.playermesh.position.x = pos.x
    this.playermesh.setDirection(Vector3.Forward(),-this.body.getAngle(), 0,0)
    const vel = this.body.getLocalVector(this.body.getLinearVelocity())

    this.wheelPos += vel.y * dT *0.2
    this.rearWheels.forEach(w=>{ w.setDirection(Vector3.Forward(),0,this.wheelPos,0) })
    this.frontWheels.forEach(w=>{ w.setDirection(Vector3.Forward(),this.turnRate *-0.15 * Math.PI ,this.wheelPos,0) })

  }

  public postPhysics(){
    const pos = this.body.getPosition()
    this.position+=pos.y
    this.body.setPosition(Vec2(pos.x,0))
  }

  public prePhysics(road:IRoadData){

    //apply turn force
    const turn = road.turnAtAccurate(this.position)
    const vel = this.body.getLocalVector(this.body.getLinearVelocity())
    const ang = this.body.getAngle()
    const speed = this.getSpeed()


    const roadRadius = (1/( turn + (0.1* this.body.getPosition().x) ) * 1000)


    switch(this.turnDirection){
      case -1:
        this.turnRate -= 0.025
        break
        
      case 1:
        this.turnRate += 0.025
        break
        
      case 0:
        //correct steering to centre
        this.turnRate = MathHelper.lerp(this.turnRate,-ang, 0.025)
        /*
        if (ang < -0.05){
          this.turnRate = MathHelper.lerp(this.turnRate,-ang, 0.05)
        }
        else if (ang > 0.05){
          
        }

        */
        
      break;
    }

    this.turnRate  = MathHelper.clamp(this.turnRate, -0.5,0.5)

    const corner  = turn  * speed * 0.0001
    this.body.setAngularVelocity(this.turnRate - corner)
 
/*
    if (this.turnDirection==0){
      //correct steering back to centre
      if (ang < -0.05){
        this.body.setAngularVelocity(0.5)
      }
      else if (ang > 0.05){
        this.body.setAngularVelocity(-0.5)
      }
      else{
        this.body.setAngle(0)    
      }
    }
*/
    //accelleration / braking
    const acc = this.acc * 500
    this.body.applyForceToCenter(Vec2(Math.sin(-ang) * acc, Math.cos(-ang) * acc))

    //limit lateral drift
    const rightNormal = this.body.getWorldVector(Vec2(1,0))
    const lateralVel = Vec2.mul(rightNormal, Vec2.dot(rightNormal,this.body.getLinearVelocity()))
    this.body.applyLinearImpulse(lateralVel.mul(-this.body.getMass() * 0.2), this.body.getWorldCenter())




    this.body.applyForceToCenter(Vec2((this.body.getMass() * speed * speed) / roadRadius , 0))

  }


  static LoadAssets(assMan:AssetsManager){
    const task = assMan.addMeshTask("loadLambo","","assets/", "lambo3.gltf")
    task.onSuccess = (task)=>{ 
      PlayerCar.carMesh = task.loadedMeshes.filter(v=>v.id == "LAMBO")[0]

      const mat = PlayerCar.carMesh.material as PBRMaterial
      mat.clearCoat.isEnabled = true
    }
  } 
}