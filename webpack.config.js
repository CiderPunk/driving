
var path = require('path');
const webpack = require('webpack');

module.exports = 
{
  mode: "development",
  watchOptions: {
    poll: true,
    ignored: /node_modules/
  },
  devtool: "inline-source-map",
  entry: "./src/index.ts",
  target: "web",
  output: {
    filename: "driving.js",
    path: path.resolve(__dirname, 'public/scripts')
  },
  watch:true,
  resolve: {
    // Add `.ts` and `.tsx` as a resolvable extension.
    extensions: [".ts", ".tsx", ".js"]
  },
  module: {
    rules: [
      { test: /\.tsx?$/, loader: "ts-loader" }
    ]
  },
  plugins: [
    new webpack.ProvidePlugin({
          'planck': 'planck-js'
      })],
  externals: {
    jquery: 'jQuery'
  }
}